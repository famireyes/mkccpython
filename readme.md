# Can Kids Code?

## Learning to Code with Python
* [Youtube Playlist](https://www.youtube.com/playlist?list=PLsk-HSGFjnaGe7sS_4VpZoEtZF2VoWtoR)

---

### Week 1
* What is Programming? - Lesson 1-1
* Drawing with Turtles - Lesson 1-2
* Variables - Lesson 1-3
* Loops - Lesson 1-4

### Week 2
* Saving and Running Programs - Lesson 1-5
* Functions - Lesson 1-6
* Input and Conditional Statements - Lesson 1-7
* Number Guessing Game - Lesson 1-8
* [Homework](https://docs.google.com/document/d/17SbV6D7-y4QYg3xB08rZhyWnFO-DT0VcVXSgRTz2prI/edit?usp=sharing)
  * hw1.py: Functions
  * hw2.py: Guessing game

### Week 3
* Rock Paper Scissors - Lesson 1-9
  * hw3.py: Rock Paper Scissors with score
* Secret Code - Lesson 1-10
  * hw4.py: Caesar Cypher
* [What do programmers actually do?](https://www.youtube.com/watch?v=g4a7_HH9Wbg)

### Week 4
* Creating Computer Graphics - Lesson 2-1
* Simple Animation - Lesson 2-2
* Animating More Objects - Lesson 2-3
* More Fun with Animation - Lesson 2-4

### Week 5
* Machine Excercise 1

---
## Lua for Beginners

* [Youtube Playlist](https://www.youtube.com/playlist?list=PL9URkxPt-PndpZlw8m_NHh0vUBU5J-BRF)

---

### Week 6
* Setup
* Variables and Types
* Functions
* Arguments
* Homework lua/hw1.lua

### Week 7
* Basic Math
* Decisions
* Boolean Math
* Loops
* Homework lua/hw1.lua

### Week 8
* Machine Excercise 2
  * Rock Paper Scissors game
  * Name and age

### Week 9
[Youtube Playlist](https://www.youtube.com/playlist?list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ)

* [Variables](https://www.youtube.com/watch?v=bpe6I_flMfo&list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ&index=2)
* [Tables](https://www.youtube.com/watch?v=7Oxh7MJAd5k&list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ&index=5)
* [Local](https://www.youtube.com/watch?v=Nw7BAHoGH9Q&list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ&index=8)
* [Built-in Libraries](https://www.youtube.com/watch?v=CgfDH9b9q3A&list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ&index=9)
* [Higher or Lower Game](https://www.youtube.com/watch?v=D8eThtXfoIY&list=PLyQg3m0a5UitP1lHptwWc_4N2ZkRQBfrZ&index=10)

### Week 10
* Machine Excercise 10
  * TBD (To be determined)

### Next steps
* [Simple Programming Problems](https://adriann.github.io/programming_problems.html)
* [How To Script On Roblox for Beginners](https://www.youtube.com/playlist?list=PLsbxI7NIoTth8CE_os8sog72YTMLPhDSf)

---
