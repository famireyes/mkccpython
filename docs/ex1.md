# Python Machine Excercise 1

## Part 1 - Basic
* Write a program to ask the user's name and age.
* Then based on the age, write a message about what the user can watch. Message will be based on:

| Age | Message |
|-----|---------|
| Less than 13 | You're just a kid, watch Peppa |
| Less than 16 | You're not old enough to watch Fresh Prince |
| Less than 18 | Some shows are still not allowed for you |
| Older | You're old, you can watch anything |

| | |
|-|-|
| Sample run | ![Sample run](ex1-basic.png) |


## Part 2 - Functions
* Define a function that will print the message based on the age
```
    def printMessage(age):
        <Replace with your code>
        <clue: use if / elif / else>
```
* And update the program to call that function

## Part 3 - While loop
* Modify the program to loop. Keep asking for the name and age * Exit the look only when the age is >= 18


| | |
|-|-|
| Sample run | ![Sample run](ex1-loop.png) |

* Program exit because age (40) is greater than 18
