# Git Cheat Sheet

| # | Action | Command |
| - |--------|---------|
| 1 | Change Directory | `cd ~/Documents/mkccpython` |
| 2 | Check project status | `git status` |
| 3 | Track changes | `git add .` |
| 4 | Check status (again) | `git status` |
| 5 | Commit changes with message | `git commit -m "describe your changes"` |
| 6 | Send updates to bitbucket | `git push` |

