from tkinter import *
import random
import time

WIDTH = 600
HEIGHT = 400
COLORS = ["red", "green", "cyan", "blue", "orange", "yellow", "magenta"]

tk = Tk()
canvas = Canvas(tk, width=WIDTH, height=HEIGHT)
tk.title("Animation 101")
canvas.pack()

class Ball(object):
    def __init__(self, size, color):
        self.shape = canvas.create_oval(10, 10, size, size, fill=color)
        self.x_speed = random.randrange(5, 20)
        self.y_speed = random.randrange(5, 20)

    def move(self):
        canvas.move(self.shape, self.x_speed, self.y_speed)
        pos = canvas.coords(self.shape)
        if pos[3] >= HEIGHT or pos[1] <= 0:
            self.y_speed = -self.y_speed
        if pos[2] >= WIDTH or pos[0] <= 0:
            self.x_speed = -self.x_speed

balls = []
for i in range(25):
    balls.append(Ball(random.randrange(20,60), random.choice(COLORS)))

while True:
    for ball in balls:
        ball.move()
    tk.update()
    time.sleep(0.05)

tk.mainloop()







