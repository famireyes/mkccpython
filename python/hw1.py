# hw1.py
import turtle
pen = turtle.Pen()

# Replace the println stateements with the actual function definition.

def square(color, size, left, top):
    pen.up()
    pen.goto(left, top)
    pen.down()
    pen.color(color)
    print('replace with for loop in lesson 1-4 Loops')

def eight_star(color, size, top, left):
    print('???')

def rose():
    pass

def spiral():
    pass
